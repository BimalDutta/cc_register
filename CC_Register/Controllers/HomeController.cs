﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;

namespace CC_Register.Controllers
{
    public class HomeController : Controller
    {
        DAL.Cascading ddl = new DAL.Cascading();
        private readonly IConfiguration _configuration;

        public HomeController(IConfiguration configuration)
        {
            this._configuration = configuration;
        }
        // GET: HomeController
        public IActionResult Index()
        {
            using (SqlConnection con = new SqlConnection(_configuration.GetConnectionString("DbConnection")))
            {
                con.Open();
                SqlCommand com = new SqlCommand("SP_getCategory", con);
                com.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(com);
                DataSet ds = new DataSet();
                da.Fill(ds);
                // DataSet ds = ddl.getDistrict();
                List<SelectListItem> list = new List<SelectListItem>();
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    list.Add(new SelectListItem { Text = dr["categoryName"].ToString(), Value = dr["category_ID"].ToString() });
                }
                ViewBag.DistrictList = list;
            }
            return View();
        }

        // GET: HomeController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: HomeController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: HomeController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: HomeController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: HomeController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: HomeController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: HomeController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        [HttpPost]
        [Microsoft.AspNetCore.Authorization.AllowAnonymous]
        public JsonResult Insert(string Catagory,string stRange,string stBaseColor,string  stVal)
        {
            bool bRst = false;
            using (SqlConnection con = new SqlConnection(_configuration.GetConnectionString("DbConnection")))
            {
                con.Open();
                SqlCommand com = new SqlCommand("InsertColData", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@category",int.Parse(Catagory));
                com.Parameters.AddWithValue("@Range", stRange);
                com.Parameters.AddWithValue("@BaseColor", stBaseColor);
                com.Parameters.AddWithValue("@StrValue", stVal);
                //con.Open();
                int i = com.ExecuteNonQuery();
                con.Close();
                if (i >= 1)
                {

                    return Json(true);

                }
                else
                {

                    return Json(false);
                }
            }
           
        }
    }
}
